---
description: >-
  Tomsawyer allows you to connect any Open Charge Point Protocol compatible EV
  charge station to your application without spending weeks and months building
  and maintaining your own Central System.
---

# ⚡ Tomsawyer CSMS API

Tomsawyer goes one step further by wrapping a RESTful API around OCPP(Open Charge Point Protocol). It also has an additional layer of Charging Intelligence to help your team focus on the customer's behaviour rather than building the infrastructure. For e.g. Tomsawyer automatically shuts off a driver when they hit a predetermined energy threshold developed by the developer.

### **Server Side REST API**

Tomsawyer is an M2M server side API that is capable of connecting mobile devices directly to Tomsawyer CSMS which internally communicates with Tomsawyer registered Chargers (limited as of now). Developers will be able to leverage Tomsawyer as a means for their client facing app and its users or drivers to connect with various services offered by a Charger via Tomsawyer defined REST API’s. For eg. “Start Charging”, “Stop Charging” etc.

All Tomsawyer API requests and responses use REST and HTTP. This allows you to use any HTTP client or programming language to interact with the API. All requests and responses are formatted in JSON- a standard format representing data objects, character strings, integers, floats, true/false values, etc.

Feeling like an eager beaver? Jump in to the quick start docs and get making your first request:

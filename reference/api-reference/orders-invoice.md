# 📄 Orders/Invoice



### Order List

{% swagger method="get" path="" baseUrl="/users/order" summary="" %}
{% swagger-description %}

{% endswagger-description %}

{% swagger-parameter in="query" name="status" type="String" %}
"PAID"
{% endswagger-parameter %}

{% swagger-parameter in="query" name="amount" type="Number" %}

{% endswagger-parameter %}

{% swagger-parameter in="query" name="type" type="String" %}

{% endswagger-parameter %}

{% swagger-parameter in="query" name="SessionBillingId" type="String" %}

{% endswagger-parameter %}

{% swagger-response status="200: OK" description="" %}
```javascript
{
    // Response
}
```
{% endswagger-response %}

{% swagger-response status="400: Bad Request" description="" %}
```javascript
{
    // Response
}
```
{% endswagger-response %}

{% swagger-response status="401: Unauthorized" description="" %}
```javascript
{
    // Response
}
```
{% endswagger-response %}

{% swagger-response status="500: Internal Server Error" description="" %}
```javascript
{
    // Response
}
```
{% endswagger-response %}
{% endswagger %}

{% tabs %}
{% tab title="HTTP" %}
```
GET //users/order?order=[['createdAt', 'DESC']] HTTP/1.1
Host: csms-api.tomsawyer-energy.com
Authorization: Bearer x.x.x

```
{% endtab %}

{% tab title="Python" %}
```
import requests

url = "https://csms-api.tomsawyer-energy.com//users/order?order=[['createdAt', 'DESC']]"

payload = ""
headers = {
  'Authorization': 'Bearer x.x.x'
}

response = requests.request("GET", url, headers=headers, data=payload)

print(response.text)

```
{% endtab %}
{% endtabs %}

### Order Detail

{% swagger method="get" path="" baseUrl="/users/order" summary="" %}
{% swagger-description %}

{% endswagger-description %}

{% swagger-parameter in="path" name="orderid" required="true" type="String" %}

{% endswagger-parameter %}

{% swagger-response status="200: OK" description="" %}
```javascript
{
    // Response
}
```
{% endswagger-response %}

{% swagger-response status="400: Bad Request" description="" %}
```javascript
{
    // Response
}
```
{% endswagger-response %}

{% swagger-response status="401: Unauthorized" description="" %}
```javascript
{
    // Response
}
```
{% endswagger-response %}

{% swagger-response status="404: Not Found" description="" %}
```javascript
{
    // Response
}
```
{% endswagger-response %}

{% swagger-response status="500: Internal Server Error" description="" %}
```javascript
{
    // Response
}
```
{% endswagger-response %}
{% endswagger %}

{% tabs %}
{% tab title="HTTP" %}
```
GET //users/order/order_IN_ztfikHEXz2 HTTP/1.1
Host: csms-api.tomsawyer-energy.com
Authorization: Bearer Bearer x.x.x
Content-Type: application/json
Content-Length: 75

{ 
    "amount":1000,
    "type":"INSTANT_CHARGE",
    "limitType":"TIME"
}

```
{% endtab %}

{% tab title="Python" %}
```
import requests
import json

url = "https://csms-api.tomsawyer-energy.com//users/order/order_IN_ztfikHEXz2"

payload = json.dumps({
  "amount": 1000,
  "type": "INSTANT_CHARGE",
  "limitType": "TIME"
})
headers = {
  'Authorization': 'Bearer Bearer x.x.x',
  'Content-Type': 'application/json'
}

response = requests.request("GET", url, headers=headers, data=payload)

print(response.text)

```
{% endtab %}
{% endtabs %}




# 💡 Charging

### Start Charging

{% swagger method="post" path="" baseUrl="/users/startCharge" summary="" %}
{% swagger-description %}

{% endswagger-description %}

{% swagger-parameter in="body" name="billingType" required="true" type="String" %}






Value will be always "INSTANT\_CHARGE"
{% endswagger-parameter %}

{% swagger-parameter in="body" name="ConnectorId" required="true" type="String" %}
e.g "connector_ECTxKihKbW"
{% endswagger-parameter %}

{% swagger-parameter in="body" name="limit" required="true" type="Number" %}
e.g 10
{% endswagger-parameter %}

{% swagger-parameter in="body" name="limitType" required="true" type="String" %}
e.g "KWH" or "AMOUNT"
{% endswagger-parameter %}

{% swagger-response status="201: Created" description="successfully done" %}
```javascript
{
    "error": null,
    "count": 1,
    "data": {
        "order": {
            "id": "order_Y7GCknSrAw",
            "amount": 160,
            "tax": null,
            "gatewayId": null,
            "gatewayName": null,
            "type": "INSTANT_CHARGE",
            "limitType": "KWH",
            "property": "{\"limit\":10,\"ChargerId\":\"localtest\",\"ConnectorId\":\"connector_ECTxKihKbW\"}",
            "status": "INPROCESS",
            "createdAt": "2022-05-13T10:30:16.243Z",
            "updatedAt": "2022-05-13T10:30:16.243Z",
            "SessionBillingId": null,
            "PlanId": null,
            "PlanUserId": null,
            "PaymentInfoId": null,
            "UserId": null,
            "zone": "IN"
        },
        "gatewayObject": {
            "id": "pi_3KyvlJSExtuRJZD10cPz5647",
            "object": "payment_intent",
            "amount": 160,
            "amount_capturable": 0,
            "amount_details": {
                "tip": {}
            },
            "amount_received": 0,
            "application": null,
            "application_fee_amount": null,
            "automatic_payment_methods": null,
            "canceled_at": null,
            "cancellation_reason": null,
            "capture_method": "automatic",
            "charges": {
                "object": "list",
                "data": [],
                "has_more": false,
                "total_count": 0,
                "url": "/v1/charges?payment_intent=pi_3KyvlJSExtuRJZD10cPz5647"
            },
            "client_secret": "pi_3KyvlJSExtuRJZD10cPz5647_secret_5sAJQG9hCOOJatAIKX9PKcrIB",
            "confirmation_method": "automatic",
            "created": 1652437821,
            "currency": "usd",
            "customer": "cus_LgIDOHBXl2NOPM",
            "description": "Software services",
            "invoice": null,
            "last_payment_error": null,
            "livemode": false,
            "metadata": {
                "zone": "IN",
                "id": "order_Y7GCknSrAw"
            },
            "next_action": null,
            "on_behalf_of": null,
            "payment_method": null,
            "payment_method_options": {
                "card": {
                    "installments": null,
                    "mandate_options": null,
                    "network": null,
                    "request_three_d_secure": "automatic"
                }
            },
            "payment_method_types": [
                "card"
            ],
            "processing": null,
            "receipt_email": null,
            "review": null,
            "setup_future_usage": null,
            "shipping": null,
            "source": null,
            "statement_descriptor": null,
            "statement_descriptor_suffix": null,
            "status": "requires_payment_method",
            "transfer_data": null,
            "transfer_group": null
        },
        "connector": {
            "id": "connector_ECTxKihKbW",
            "connectorId": 1,
            "status": "Available",
            "type": "CCS2",
            "createdAt": "2021-09-14T12:21:08.941Z",
            "updatedAt": "2022-05-13T10:30:08.696Z",
            "ChargerId": "localtest",
            "Charger": {
                "online": true,
                "id": "localtest",
                "name": "test charger",
                "description": "vmvh",
                "pincode": "34567",
                "city": "Delhi",
                "region": null,
                "address": null,
                "identification": "Near house",
                "location": {
                    "type": "Point",
                    "coordinates": [
                        28.6129167,
                        77.227321
                    ]
                },
                "openTime": "00:00",
                "closeTime": "24:00",
                "kw": 7.5,
                "meterValueInterval": 12,
                "lastHeartbeat": "2022-05-13T10:30:08.798Z",
                "lastCsms": null,
                "vendor": "",
                "model": "",
                "enabled": true,
                "pricePerKwh": 20,
                "pricePerMinute": 15,
                "currency": "USD",
                "verified": true,
                "isPremium": false,
                "vehicleType": "4",
                "type": "AC",
                "createdAt": "2021-09-14T12:21:08.032Z",
                "updatedAt": "2022-05-13T10:30:09.009Z",
                "UserId": "user_G2T9AdM",
                "ChargerCommissionGroupId": "ChargerCommissionGroup_gpQA9ksfER",
                "ChargerGroupId": "PublicList"
            }
        },
        "idTag": "in:hkjd790ac",
        "limit": 10,
        "limitType": "KWH"
    },
    "status": 201
}
```
{% endswagger-response %}

{% swagger-response status="400: Bad Request" description="" %}
```javascript
{
    // Response
}
```
{% endswagger-response %}

{% swagger-response status="401: Unauthorized" description="" %}
```javascript
{
    // Response
}
```
{% endswagger-response %}

{% swagger-response status="404: Not Found" description="" %}
```javascript
{
    // Response
}
```
{% endswagger-response %}

{% swagger-response status="500: Internal Server Error" description="" %}
```javascript
{
    // Response
}
```
{% endswagger-response %}
{% endswagger %}

{% tabs %}
{% tab title="HTTP" %}
```
POST //users/startCharge HTTP/1.1
Host: csms-api.tomsawyer-energy.com
Authorization: Bearer x.x.x
Content-Type: application/json
Content-Length: 121

{
    "billingType":"INSTANT_CHARGE",
    "ConnectorId":"connector_ECTxKihKbW", 
    "limit":10, 
    "limitType":"KWH"
}

```
{% endtab %}

{% tab title="Python" %}
```
import requests
import json

url = "https://csms-api.tomsawyer-energy.com//users/startCharge"

payload = json.dumps({
  "billingType": "INSTANT_CHARGE",
  "ConnectorId": "connector_ECTxKihKbW",
  "limit": 10,
  "limitType": "KWH"
})
headers = {
  'Authorization': 'Bearer x.x.x',
  'Content-Type': 'application/json'
}

response = requests.request("POST", url, headers=headers, data=payload)

print(response.text)

```
{% endtab %}
{% endtabs %}

### Stop Charging

{% swagger method="post" path="" baseUrl="/users/stopCharge" summary="" %}
{% swagger-description %}

{% endswagger-description %}

{% swagger-parameter in="body" name="ConnectorId" required="true" %}









{% endswagger-parameter %}

{% swagger-response status="200: OK" description="successfully done" %}
```javascript
{
    // Response
}
```
{% endswagger-response %}

{% swagger-response status="400: Bad Request" description="" %}
```javascript
{
    // Response
}
```
{% endswagger-response %}

{% swagger-response status="401: Unauthorized" description="" %}
```javascript
{
    // Response
}
```
{% endswagger-response %}

{% swagger-response status="404: Not Found" description="" %}
```javascript
{
    // Response
}
```
{% endswagger-response %}

{% swagger-response status="500: Internal Server Error" description="" %}
```javascript
{
    // Response
}
```
{% endswagger-response %}
{% endswagger %}

{% tabs %}
{% tab title="HTTP" %}
```
POST /users/stopCharge HTTP/1.1
Host: csms-api.tomsawyer-energy.com
Authorization: Bearer x.x.x
Content-Type: application/json
Content-Length: 44

{
    "ConnectorId":"connector_ECTxKihKbW"
}
```
{% endtab %}

{% tab title="Python" %}
```
import requests
import json

url = "csms-api.tomsawyer-energy.com/users/stopCharge"

payload = json.dumps({
  "ConnectorId": "connector_ECTxKihKbW"
})
headers = {
  'Authorization': 'Bearer x.x.x',
  'Content-Type': 'application/json'
}

response = requests.request("POST", url, headers=headers, data=payload)

print(response.text)

```
{% endtab %}
{% endtabs %}

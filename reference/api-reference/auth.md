# 🔑 Auth

### Signup

{% swagger method="post" path="" baseUrl="/common/auth/signup" summary="" %}
{% swagger-description %}

{% endswagger-description %}

{% swagger-parameter in="body" name="phone" required="true" %}
\+919876543210
{% endswagger-parameter %}

{% swagger-parameter in="body" name="password" required="true" %}
qwertyuiop
{% endswagger-parameter %}

{% swagger-parameter in="body" name="confirmPassword" required="true" %}
qwertyuiop
{% endswagger-parameter %}

{% swagger-response status="200: OK" description="" %}
```javascript
{
    "error": null,
    "data": {
        "user": {
            "needToChangePassword": false,
            "phone": "+919854425366",
            "createdAt": "2022-06-16T08:28:38.143Z",
            "updatedAt": "2022-06-16T08:28:38.143Z",
            "phoneVerified": false,
            "emailVerified": false,
            "id": "user_f4_BvKb",
            "name": null,
            "email": null,
            "googleUserId": null,
            "facebookUserId": null,
            "paymentGatewayCustomerId": null,
            "address": null,
            "city": null,
            "pincode": null,
            "MediumId": null,
            "HostApprovalId": null,
            "Tester": null,
            "Assignee": null
        },
      },
    "status": 201
}
```
{% endswagger-response %}

{% swagger-response status="400: Bad Request" description="" %}
```javascript
{
    // Response
}
```
{% endswagger-response %}

{% swagger-response status="500: Internal Server Error" description="" %}
```javascript
{
    // Response
}
```
{% endswagger-response %}
{% endswagger %}

{% tabs %}
{% tab title="HTTP" %}
```
POST //common/auth/signup HTTP/1.1
Host: csms-api.tomsawyer-energy.com
country: IN
Content-Type: application/json
Content-Length: 96

{
    "password":"qwertyuiop",
    "confirmPassword":"qwertyuiop",
    "phone":"+919876543210"
}

```
{% endtab %}

{% tab title="Python" %}
```
import requests
import json

url = "https://csms-api.tomsawyer-energy.com/common/auth/signup"

payload = json.dumps({
  "password": "qwertyuiop",
  "confirmPassword": "qwertyuiop",
  "phone": "+919876543210"
})
headers = {
  'country': 'IN',
  'Content-Type': 'application/json'
}

response = requests.request("POST", url, headers=headers, data=payload)

print(response.text)

```
{% endtab %}
{% endtabs %}

### Login (Passwordless) Step 1

{% swagger method="post" path="/" baseUrl="/common/auth/sendOtp" summary="" %}
{% swagger-description %}

{% endswagger-description %}

{% swagger-parameter in="body" name="type" required="true" %}
"CUSTOMER_PASSWORDLESS_LOGIN"
{% endswagger-parameter %}

{% swagger-parameter in="body" name="phone" required="true" %}
\+919876543210
{% endswagger-parameter %}

{% swagger-response status="200: OK" description="" %}
```javascript
{
    "error": null,
    "count": 1,
    "data": {
        "message": "success"
    },
    "status": 200
}
```
{% endswagger-response %}

{% swagger-response status="400: Bad Request" description="" %}
```javascript
{
    // Response
}
```
{% endswagger-response %}

{% swagger-response status="401: Unauthorized" description="" %}
```javascript
{
    // Response
}
```
{% endswagger-response %}

{% swagger-response status="500: Internal Server Error" description="" %}
```javascript
{
    // Response
}
```
{% endswagger-response %}
{% endswagger %}



{% tabs %}
{% tab title="HTTP" %}
```
POST //common/auth/sendOtp HTTP/1.1
Host: csms-api.tomsawyer-energy.com
Content-Type: application/json
Content-Length: 76

{
    "otpType":"CUSTOMER_PASSWORDLESS_LOGIN",
    "phone":"+919876543210"
}

```
{% endtab %}

{% tab title="Python" %}
```
import requests
import json

url = "https://csms-api.tomsawyer-energy.com/common/auth/sendOtp"

payload = json.dumps({
  "otpType": "CUSTOMER_PASSWORDLESS_LOGIN",
  "phone": "+919876543210"
})
headers = {
  'Content-Type': 'application/json'
}

response = requests.request("POST", url, headers=headers, data=payload)

print(response.text)

```
{% endtab %}
{% endtabs %}

### Login (Passwordless) Step 2



{% swagger method="post" path="" baseUrl="/common/auth/passwordLessLogin" summary="" %}
{% swagger-description %}

{% endswagger-description %}

{% swagger-parameter in="body" name="code" required="true" %}
"1234"
{% endswagger-parameter %}

{% swagger-parameter in="body" required="true" name="phone" %}
"+919876543210"
{% endswagger-parameter %}

{% swagger-response status="200: OK" description="" %}
```javascript
{
{
    "error": null,
    "data": {
        "user": {
            "id": "user_LaI6Gkp",
            "name": "d d",
            "password": "$2a$10$0HF.UzOp/mmvWDZNU0kEouo/zXLVOW2.kXBR1YORqebYAzOeAee9C",
            "needToChangePassword": false,
            "phone": "919876543210",
            "phoneVerified": true,
            "email": "xx@tomsawyer.co",
            "emailVerified": true,
            "googleUserId": null,
            "facebookUserId": null,
            "paymentGatewayCustomerId": "cus_KypQiytOHmMn82",
            "address": null,
            "city": null,
            "pincode": null,
            "createdAt": "2021-12-05T19:10:26.503Z",
            "updatedAt": "2022-02-25T07:18:10.563Z",
            "MediumId": null,
            "HostApprovalId": null,
            "Tester": null,
            "Assignee": null,
            "role": []
        },
        "token": {
            "accessToken": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6InVzZXJfTGFJNkdrcCIsIm5hbWUiOiJQcml5YW5rdSBoYXphcmlrYSIsIm5lZWRUb0NoYW5nZVBhc3N3b3JkIjpmYWxzZSwicGhvbmUiOiI5MTcwMDIzMjMzMjgiLCJwaG9uZVZlcmlmaWVkIjp0cnVlLCJlbWFpbCI6InByaXlhbmt1QHRvbXNhd3llci5jbyIsImVtYWlsVmVyaWZpZWQiOnRydWUsImdvb2dsZVVzZXJJZCI6bnVsbCwiZmFjZWJvb2tVc2VySWQiOm51bGwsInBheW1lbnRHYXRld2F5Q3VzdG9tZXJJZCI6ImN1c19LeXBRaXl0T0htTW44MiIsImFkZHJlc3MiOm51bGwsImNpdHkiOm51bGwsInBpbmNvZGUiOm51bGwsImNyZWF0ZWRBdCI6IjIwMjEtMTItMDVUMTk6MTA6MjYuNTAzWiIsInVwZGF0ZWRBdCI6IjIwMjItMDItMjVUMDc6MTg6MTAuNTYzWiIsIk1lZGl1bUlkIjpudWxsLCJIb3N0QXBwcm92YWxJZCI6bnVsbCwiVGVzdGVyIjpudWxsLCJBc3NpZ25lZSI6bnVsbCwicm9sZSI6W1t7ImlkIjoiQ1VTVE9NRVIiLCJyb2xlTmFtZSI6IkNVU1RPTUVSIiwiYWN0aXZlIjp0cnVlLCJjcmVhdGVkQXQiOiIyMDIxLTA5LTE0VDE0OjExOjAwLjAwMFoiLCJ1cGRhdGVkQXQiOiIyMDIxLTA5LTE0VDE0OjExOjAwLjAwMFoiLCJVc2VyY291bnRyeVJvbGVzIjp7ImNyZWF0ZWRBdCI6IjIwMjEtMTItMDVUMTk6MTA6MjcuMzE3WiIsInVwZGF0ZWRBdCI6IjIwMjEtMTItMDVUMTk6MTA6MjcuMzE3WiIsIlJvbGVJZCI6IkNVU1RPTUVSIiwiVXNlcmNvdW50cnlVc2VySWQiOiJ1c2VyX0xhSTZHa3AifX0seyJpZCI6InJvbGVfN0RFazkiLCJyb2xlTmFtZSI6Ik1hc3RlciBBZG1pbiIsImFjdGl2ZSI6dHJ1ZSwiY3JlYXRlZEF0IjoiMjAyMS0xMS0yOVQxODowODoxNy45NThaIiwidXBkYXRlZEF0IjoiMjAyMS0xMS0yOVQxODowODoxNy45NThaIiwiVXNlcmNvdW50cnlSb2xlcyI6eyJjcmVhdGVkQXQiOiIyMDIxLTEyLTA1VDE5OjEwOjI3Ljk3NFoiLCJ1cGRhdGVkQXQiOiIyMDIxLTEyLTA1VDE5OjEwOjI3Ljk3NFoiLCJSb2xlSWQiOiJyb2xlXzdERWs5IiwiVXNlcmNvdW50cnlVc2VySWQiOiJ1c2VyX0xhSTZHa3AifX1dXSwicm9sZXMiOlt7IlJvbGVJZCI6IkNVU1RPTUVSIn0seyJSb2xlSWQiOiJyb2xlXzdERWs5In1dLCJ6b25lIjoiSU4iLCJ0eXBlIjoiYWNjZXNzIiwiaWF0IjoxNjU1MzY4MjczLCJleHAiOjE2NTUzNzE4NzMsImlzcyI6IkNTTVMgW0RFVl0iLCJzdWIiOiJ1c2VyX0xhSTZHa3AifQ.V1KHqFqsKMLUrZtsjJg-B6ul-M59HBpPiFeWvHb4T38",
            "refreshToken": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6InVzZXJfTGFJNkdrcCIsImVtYWlsIjoicHJpeWFua3VAdG9tc2F3eWVyLmNvIiwidHlwZSI6InJlZnJlc2giLCJpYXQiOjE2NTUzNjgyNzMsImV4cCI6MTY1Nzk2MDI3MywiaXNzIjoiQ1NNUyBbREVWXSIsInN1YiI6InVzZXJfTGFJNkdrcCJ9.nSgE6ftyEhitsG-pxyMmIddR4o6-fAFLuwOtpyil9Fs"
        }
    },
    "status": 200
}
```
{% endswagger-response %}

{% swagger-response status="400: Bad Request" description="" %}
```javascript
{
    // Response
}
```
{% endswagger-response %}

{% swagger-response status="401: Unauthorized" description="" %}
```javascript
{
    // Response
}
```
{% endswagger-response %}

{% swagger-response status="500: Internal Server Error" description="" %}
```javascript
{
    // Response
}
```
{% endswagger-response %}
{% endswagger %}

{% tabs %}
{% tab title="HTTP" %}
```
POST /common/auth/passwordLessLogin HTTP/1.1
Host: localhost:6002
Content-Type: application/json
Content-Length: 50

{
    "code":"2701",
    "phone":"+919876543210"
}
```
{% endtab %}

{% tab title="Python" %}
```
import requests
import json

url = "http://localhost:6002/common/auth/passwordLessLogin"

payload = json.dumps({
  "code": "2701",
  "phone": "+9198765543210"
})
headers = {
  'Content-Type': 'application/json'
}

response = requests.request("POST", url, headers=headers, data=payload)

print(response.text)
```
{% endtab %}
{% endtabs %}




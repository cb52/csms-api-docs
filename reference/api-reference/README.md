# API Reference

Dive into the specifics of each API endpoint by checking out our complete documentation.

## Chargers

{% content-ref url="Chargers.md" %}
[Chargers.md](Chargers.md)
{% endcontent-ref %}

## Users

Everything related to users:

{% content-ref url="profile.md" %}
[profile.md](profile.md)
{% endcontent-ref %}

{% hint style="info" %}
**Good to know:** Using the 'Page Link' block lets you link directly to a page. If this page's name, URL or parent location changes, the reference will be kept up to date. You can also mention a page – like [Chargers.md](Chargers.md "mention") – if you don't want a block-level link.
{% endhint %}

# 📊 Meter values

### Get Meter values

{% swagger method="get" path="" baseUrl="/users/metervalues/order/" summary="" %}
{% swagger-description %}

{% endswagger-description %}

{% swagger-parameter in="path" name="order id" required="true" type="String" %}

{% endswagger-parameter %}

{% swagger-response status="200: OK" description="successfully done" %}
```javascript
{
    "error": null,
    "data": {
        "voltage": 246.24,
        "current": 58,
        "temperature": 25,
        "soc": 7,
        "percentage": 100,
        "energy": 7,
        "estimatedTimeRemainingInMinutes": 80,
        "timePassedInSeconds": 294826,
        "isChargingStopped": false
    },
    "status": 200
}
```
{% endswagger-response %}

{% swagger-response status="400: Bad Request" description="" %}
```javascript
{
    // Response
}
```
{% endswagger-response %}

{% swagger-response status="401: Unauthorized" description="" %}
```javascript
{
    // Response
}
```
{% endswagger-response %}

{% swagger-response status="500: Internal Server Error" description="" %}
```javascript
{
    // Response
}
```
{% endswagger-response %}
{% endswagger %}

{% tabs %}
{% tab title="HTTP" %}
```
GET /users/metervalues/order/order_621K5C0yuw HTTP/1.1
Host: https://csms-api.tomsawyer-energy.com/
Authorization: Bearer Bearer x.x.x

```
{% endtab %}

{% tab title="Python" %}
```
import requests

url = "http://localhost:6002/users/metervalues/order/order_621K5C0yuw"

payload={}
headers = {
  'Authorization': 'Bearer Bearer x.x.x'
}

response = requests.request("GET", url, headers=headers, data=payload)

print(response.text)

```
{% endtab %}
{% endtabs %}


# 🔌 Chargers

## List charger

{% swagger baseUrl="/users/charger/" method="get" path="" summary="List charger." %}
{% swagger-description %}

{% endswagger-description %}

{% swagger-parameter in="query" name="online" type="Boolean" %}

{% endswagger-parameter %}

{% swagger-parameter in="query" name="name" type="String" %}

{% endswagger-parameter %}

{% swagger-parameter in="query" name="description" type="String" %}

{% endswagger-parameter %}

{% swagger-parameter in="query" name="pincode" type="String" %}

{% endswagger-parameter %}

{% swagger-parameter in="query" name="address" type="String" %}

{% endswagger-parameter %}

{% swagger-parameter in="query" name="city" type="String" %}

{% endswagger-parameter %}

{% swagger-parameter in="query" name="kw" type="Number" %}

{% endswagger-parameter %}

{% swagger-parameter in="query" name="type" type="String" %}
Example "AC" or "DC"
{% endswagger-parameter %}

{% swagger-parameter in="query" name="location" type="Array" %}
\[lat, long]
{% endswagger-parameter %}

{% swagger-response status="200" description="successfully fetch" %}
```javascript
{
    "error": null,
    "count": 20,
    "data": [
        {
            "online": false,
            "id": "charger_dAXSLPptUK",
            "name": "test charger",
            "description": "vmvh",
            "pincode": "785614",
            "city": "Mandir",
            "address": "Gwalior",
            "identification": "Near Namghar",
            "location": {
                "type": "Point",
                "coordinates": [
                    1.3849155,
                    103.8644965
                ]
            },
            "openTime": "00:00",
            "closeTime": "24:00",
            "kw": 7.5,
            "meterValueInterval": 12,
            "lastHeartbeat": "2022-04-07T07:23:39.601Z",
            "lastCsms": null,
            "vendor": "EVRE",
            "model": "10kw",
            "enabled": true,
            "pricePerKwh": 20,
            "pricePerMinute": 15,
            "currency": "USD",
            "verified": true,
            "isPremium": false,
            "vehicleType": "4",
            "type": "AC",
            "createdAt": "2021-09-14T12:13:05.224Z",
            "updatedAt": "2022-04-07T07:23:39.601Z",
            "UserId": "user_G2T9AdM",
            "ChargerCommissionGroupId": null,
            "ChargerGroupId": "PublicList",
            "Connectors": [
                {
                    "id": "connector_Pq28q0Q1nG",
                    "connectorId": 0,
                    "status": "Available",
                    "type": "CCS",
                    "createdAt": "2022-02-26T06:11:40.048Z",
                    "updatedAt": "2022-02-26T09:11:02.884Z",
                    "ChargerId": "charger_dAXSLPptUK"
                },
                {
                    "id": "connector_5QfaLyEWVE",
                    "connectorId": 1,
                    "status": "Unavailable",
                    "type": "CCS2",
                    "createdAt": "2021-09-14T12:13:06.079Z",
                    "updatedAt": "2022-04-07T07:20:12.457Z",
                    "ChargerId": "charger_dAXSLPptUK"
                },
                {
                    "id": "connector_uf2qxy3dlj",
                    "connectorId": 2,
                    "status": "Preparing",
                    "type": "CHADEMO",
                    "createdAt": "2021-09-14T12:13:06.079Z",
                    "updatedAt": "2022-04-07T07:20:13.433Z",
                    "ChargerId": "charger_dAXSLPptUK"
                }
            ],
            "ChargerGroup": {
                "id": "PublicList",
                "name": "Test Station",
                "description": "Test",
                "type": "PublicList",
                "createdAt": "2021-10-04T17:48:35.000Z",
                "updatedAt": "2021-10-04T17:48:35.000Z"
            }
        },
        {
            "online": false,
            "id": "charger_kV2Hu0MsBu",
            "name": "test charger",
            "description": "vmvh",
            "pincode": "785614",
            "city": "Mandir",
            "address": "Kanpur",
            "identification": "Near Namghar",
            "location": {
                "crs": {
                    "type": "name",
                    "properties": {
                        "name": "EPSG:4326"
                    }
                },
                "type": "Point",
                "coordinates": [
                    26.4499,
                    80.3319
                ]
            },
            "openTime": "00:00",
            "closeTime": "24:00",
            "kw": 7.5,
            "meterValueInterval": 12,
            "lastHeartbeat": null,
            "lastCsms": null,
            "vendor": "aefd",
            "model": null,
            "enabled": true,
            "pricePerKwh": 20,
            "pricePerMinute": 15,
            "currency": "USD",
            "verified": true,
            "isPremium": false,
            "vehicleType": "4",
            "type": "AC",
            "createdAt": "2021-09-14T12:13:32.486Z",
            "updatedAt": "2021-10-04T18:10:56.791Z",
            "UserId": "user_G2T9AdM",
            "ChargerCommissionGroupId": null,
            "ChargerGroupId": "PublicList",
            "Connectors": [
                {
                    "id": "connector_AYLUK5fW-C",
                    "connectorId": 1,
                    "status": "Unavailable",
                    "type": "CCS2",
                    "createdAt": "2021-09-14T12:13:32.863Z",
                    "updatedAt": "2021-09-14T12:13:32.863Z",
                    "ChargerId": "charger_kV2Hu0MsBu"
                },
                {
                    "id": "connector_BOlRGvCAKX",
                    "connectorId": 2,
                    "status": "Unavailable",
                    "type": "CHADEMO",
                    "createdAt": "2021-09-14T12:13:32.863Z",
                    "updatedAt": "2021-09-14T12:13:32.863Z",
                    "ChargerId": "charger_kV2Hu0MsBu"
                }
            ],
            "ChargerGroup": {
                "id": "PublicList",
                "name": "Test Station",
                "description": "Test",
                "type": "PublicList",
                "createdAt": "2021-10-04T17:48:35.000Z",
                "updatedAt": "2021-10-04T17:48:35.000Z"
            }
        },
        {
            "online": false,
            "id": "localtest99",
            "name": "test charger",
            "description": "vmvh",
            "pincode": "785614",
            "city": "Mandir",
            "address": "Mandir",
            "identification": "Near Namghar",
            "location": {
                "crs": {
                    "type": "name",
                    "properties": {
                        "name": "EPSG:4326"
                    }
                },
                "type": "Point",
                "coordinates": [
                    26.5239,
                    93.9623
                ]
            },
            "openTime": "00:00",
            "closeTime": "24:00",
            "kw": 7.5,
            "meterValueInterval": 12,
            "lastHeartbeat": "2021-10-31T18:34:57.873Z",
            "lastCsms": null,
            "vendor": "P",
            "model": "PV12",
            "enabled": true,
            "pricePerKwh": 20,
            "pricePerMinute": 15,
            "currency": "USD",
            "verified": true,
            "isPremium": false,
            "vehicleType": "4",
            "type": "AC",
            "createdAt": "2021-09-27T07:05:31.374Z",
            "updatedAt": "2021-10-31T18:34:57.873Z",
            "UserId": "user_G2T9AdM",
            "ChargerCommissionGroupId": null,
            "ChargerGroupId": "PublicList",
            "Connectors": [
                {
                    "id": "connector_sQsvtcWLls",
                    "connectorId": 1,
                    "status": "SuspendedEVSE",
                    "type": "CCS2",
                    "createdAt": "2021-09-27T07:05:34.526Z",
                    "updatedAt": "2021-10-31T13:59:59.886Z",
                    "ChargerId": "localtest99"
                },
                {
                    "id": "connector_8jjNQroGQG",
                    "connectorId": 2,
                    "status": "Unavailable",
                    "type": "CHADEMO",
                    "createdAt": "2021-09-27T07:05:34.526Z",
                    "updatedAt": "2021-09-27T07:05:34.526Z",
                    "ChargerId": "localtest99"
                }
            ],
            "ChargerGroup": {
                "id": "PublicList",
                "name": "Test Station",
                "description": "Test",
                "type": "PublicList",
                "createdAt": "2021-10-04T17:48:35.000Z",
                "updatedAt": "2021-10-04T17:48:35.000Z"
            }
        },
        {
            "online": false,
            "id": "114325",
            "name": "Yen Hau Test",
            "description": "",
            "pincode": null,
            "city": null,
            "address": "",
            "identification": null,
            "location": {
                "crs": {
                    "type": "name",
                    "properties": {
                        "name": "EPSG:4326"
                    }
                },
                "type": "Point",
                "coordinates": [
                    0,
                    0
                ]
            },
            "openTime": null,
            "closeTime": null,
            "kw": null,
            "meterValueInterval": null,
            "lastHeartbeat": null,
            "lastCsms": null,
            "vendor": null,
            "model": null,
            "enabled": true,
            "pricePerKwh": 20,
            "pricePerMinute": 15,
            "currency": "USD",
            "verified": false,
            "isPremium": false,
            "vehicleType": "4",
            "type": "AC",
            "createdAt": "2022-03-15T08:07:19.663Z",
            "updatedAt": "2022-03-15T08:07:19.663Z",
            "UserId": "user_G2T9AdM",
            "ChargerCommissionGroupId": null,
            "ChargerGroupId": "PublicList",
            "Connectors": [
                {
                    "id": "connector_ILdDvxcx_G",
                    "connectorId": 1,
                    "status": "Unavailable",
                    "type": "TYPE2",
                    "createdAt": "2022-03-15T08:07:20.293Z",
                    "updatedAt": "2022-03-15T08:07:20.293Z",
                    "ChargerId": "114325"
                }
            ],
            "ChargerGroup": {
                "id": "PublicList",
                "name": "Test Station",
                "description": "Test",
                "type": "PublicList",
                "createdAt": "2021-10-04T17:48:35.000Z",
                "updatedAt": "2021-10-04T17:48:35.000Z"
            }
        },
        {
            "online": false,
            "id": "charger_OQETX-3Wr6",
            "name": "test charger",
            "description": "vmvh",
            "pincode": "785614",
            "city": "Mandir",
            "address": "Agra",
            "identification": "Near Namghar",
            "location": {
                "type": "Point",
                "coordinates": [
                    1.3688537,
                    103.8948377
                ]
            },
            "openTime": "00:00",
            "closeTime": "24:00",
            "kw": 7.5,
            "meterValueInterval": 12,
            "lastHeartbeat": "2021-11-22T04:49:17.601Z",
            "lastCsms": null,
            "vendor": "",
            "model": "",
            "enabled": true,
            "pricePerKwh": 20,
            "pricePerMinute": 15,
            "currency": "USD",
            "verified": true,
            "isPremium": false,
            "vehicleType": "4",
            "type": "AC",
            "createdAt": "2021-09-14T12:12:35.004Z",
            "updatedAt": "2021-11-22T04:49:17.601Z",
            "UserId": "user_G2T9AdM",
            "ChargerCommissionGroupId": null,
            "ChargerGroupId": "PublicList",
            "Connectors": [
                {
                    "id": "connector_GTjK4ezP62",
                    "connectorId": 1,
                    "status": "Available",
                    "type": "CCS2",
                    "createdAt": "2021-09-14T12:12:35.114Z",
                    "updatedAt": "2021-11-22T04:49:12.378Z",
                    "ChargerId": "charger_OQETX-3Wr6"
                },
                {
                    "id": "connector_ARTjQ0JkRA",
                    "connectorId": 2,
                    "status": "Available",
                    "type": "CHADEMO",
                    "createdAt": "2021-09-14T12:12:35.114Z",
                    "updatedAt": "2021-11-22T04:49:12.503Z",
                    "ChargerId": "charger_OQETX-3Wr6"
                }
            ],
            "ChargerGroup": {
                "id": "PublicList",
                "name": "Test Station",
                "description": "Test",
                "type": "PublicList",
                "createdAt": "2021-10-04T17:48:35.000Z",
                "updatedAt": "2021-10-04T17:48:35.000Z"
            }
        },
        {
            "online": false,
            "id": "dghfkxdshfg",
            "name": "test charger",
            "description": " you",
            "pincode": "785614",
            "city": "Mandir",
            "address": null,
            "identification": "Near Namghar",
            "location": {
                "crs": {
                    "type": "name",
                    "properties": {
                        "name": "EPSG:4326"
                    }
                },
                "type": "Point",
                "coordinates": [
                    27.1751,
                    78.0421
                ]
            },
            "openTime": "00:00",
            "closeTime": "24:00",
            "kw": 7.5,
            "meterValueInterval": 12,
            "lastHeartbeat": null,
            "lastCsms": null,
            "vendor": null,
            "model": null,
            "enabled": true,
            "pricePerKwh": 20,
            "pricePerMinute": 15,
            "currency": "USD",
            "verified": false,
            "isPremium": false,
            "vehicleType": "4",
            "type": "AC",
            "createdAt": "2022-03-04T10:53:14.687Z",
            "updatedAt": "2022-03-04T10:53:14.687Z",
            "UserId": "user_G2T9AdM",
            "ChargerCommissionGroupId": null,
            "ChargerGroupId": "PCTY21080055u05",
            "Connectors": [
                {
                    "id": "connector_H515mrqudO",
                    "connectorId": 1,
                    "status": "Unavailable",
                    "type": "TYPE2",
                    "createdAt": "2022-03-04T10:53:14.713Z",
                    "updatedAt": "2022-03-04T10:53:14.713Z",
                    "ChargerId": "dghfkxdshfg"
                }
            ],
            "ChargerGroup": {
                "id": "PCTY21080055u05",
                "name": "PCTY21080055u05",
                "description": null,
                "type": "Unlisted",
                "createdAt": "2022-02-18T18:16:44.801Z",
                "updatedAt": "2022-02-18T18:16:44.801Z"
            }
        },
        {
            "online": false,
            "id": "dghfkxddshfg",
            "name": "test charger",
            "description": " you",
            "pincode": "785614",
            "city": "Mandir",
            "address": null,
            "identification": "Near Namghar",
            "location": {
                "crs": {
                    "type": "name",
                    "properties": {
                        "name": "EPSG:4326"
                    }
                },
                "type": "Point",
                "coordinates": [
                    27.1751,
                    78.0421
                ]
            },
            "openTime": "00:00",
            "closeTime": "24:00",
            "kw": 7.5,
            "meterValueInterval": 12,
            "lastHeartbeat": null,
            "lastCsms": null,
            "vendor": null,
            "model": null,
            "enabled": true,
            "pricePerKwh": 20,
            "pricePerMinute": 15,
            "currency": "USD",
            "verified": false,
            "isPremium": false,
            "vehicleType": "4",
            "type": "AC",
            "createdAt": "2022-03-04T10:53:25.123Z",
            "updatedAt": "2022-03-04T10:53:25.123Z",
            "UserId": "user_G2T9AdM",
            "ChargerCommissionGroupId": null,
            "ChargerGroupId": "PCTY21080055u05",
            "Connectors": [
                {
                    "id": "connector_fbiEA4CR9d",
                    "connectorId": 1,
                    "status": "Unavailable",
                    "type": "TYPE2",
                    "createdAt": "2022-03-04T10:53:25.128Z",
                    "updatedAt": "2022-03-04T10:53:25.128Z",
                    "ChargerId": "dghfkxddshfg"
                }
            ],
            "ChargerGroup": {
                "id": "PCTY21080055u05",
                "name": "PCTY21080055u05",
                "description": null,
                "type": "Unlisted",
                "createdAt": "2022-02-18T18:16:44.801Z",
                "updatedAt": "2022-02-18T18:16:44.801Z"
            }
        },
        {
            "online": false,
            "id": "charger_Dug-RvCisl",
            "name": "Localtest",
            "description": "",
            "pincode": null,
            "city": null,
            "address": "Opposite of slice restaurant, Jail road\nJail colony",
            "identification": null,
            "location": {
                "type": "Point",
                "coordinates": [
                    27.1741498,
                    78.041021
                ]
            },
            "openTime": "00:00",
            "closeTime": "24:00",
            "kw": 7.5,
            "meterValueInterval": 12,
            "lastHeartbeat": "2021-09-15T17:01:18.772Z",
            "lastCsms": null,
            "vendor": "",
            "model": "",
            "enabled": true,
            "pricePerKwh": 20,
            "pricePerMinute": 15,
            "currency": "USD",
            "verified": true,
            "isPremium": false,
            "vehicleType": "4",
            "type": "AC",
            "createdAt": "2021-09-14T03:32:28.843Z",
            "updatedAt": "2021-09-15T17:01:18.773Z",
            "UserId": "user_wmt2ijH",
            "ChargerCommissionGroupId": null,
            "ChargerGroupId": "PublicList",
            "Connectors": [
                {
                    "id": "1",
                    "connectorId": 1,
                    "status": "Available",
                    "type": "CCS1",
                    "createdAt": "2021-09-14T03:32:29.286Z",
                    "updatedAt": "2021-09-15T16:59:33.552Z",
                    "ChargerId": "charger_Dug-RvCisl"
                }
            ],
            "ChargerGroup": {
                "id": "PublicList",
                "name": "Test Station",
                "description": "Test",
                "type": "PublicList",
                "createdAt": "2021-10-04T17:48:35.000Z",
                "updatedAt": "2021-10-04T17:48:35.000Z"
            }
        },
        {
            "online": false,
            "id": "test01",
            "name": "ABC",
            "description": "test1",
            "pincode": null,
            "city": null,
            "address": "2 Cleantech Loop, #03-05/06, LaunchPad@JID 637144",
            "identification": null,
            "location": {
                "crs": {
                    "type": "name",
                    "properties": {
                        "name": "EPSG:4326"
                    }
                },
                "type": "Point",
                "coordinates": [
                    0,
                    0
                ]
            },
            "openTime": null,
            "closeTime": null,
            "kw": null,
            "meterValueInterval": null,
            "lastHeartbeat": null,
            "lastCsms": null,
            "vendor": null,
            "model": null,
            "enabled": true,
            "pricePerKwh": 20,
            "pricePerMinute": 15,
            "currency": "USD",
            "verified": false,
            "isPremium": false,
            "vehicleType": "2",
            "type": "AC",
            "createdAt": "2022-03-30T05:14:39.042Z",
            "updatedAt": "2022-03-30T05:14:39.042Z",
            "UserId": "user_G2T9AdM",
            "ChargerCommissionGroupId": null,
            "ChargerGroupId": "ChargerGroup_wNY6EGa",
            "Connectors": [
                {
                    "id": "connector_56iOTKj9I8",
                    "connectorId": 1,
                    "status": "Unavailable",
                    "type": "CCS1",
                    "createdAt": "2022-03-30T05:14:39.068Z",
                    "updatedAt": "2022-03-30T05:14:39.068Z",
                    "ChargerId": "test01"
                }
            ],
            "ChargerGroup": {
                "id": "ChargerGroup_wNY6EGa",
                "name": "",
                "description": "",
                "type": "PublicList",
                "createdAt": "2022-03-30T05:10:04.882Z",
                "updatedAt": "2022-03-30T05:10:04.882Z"
            }
        },
        {
            "online": false,
            "id": "test02",
            "name": "Chargepoint1",
            "description": "",
            "pincode": null,
            "city": null,
            "address": "Singapore",
            "identification": null,
            "location": {
                "crs": {
                    "type": "name",
                    "properties": {
                        "name": "EPSG:4326"
                    }
                },
                "type": "Point",
                "coordinates": [
                    0,
                    0
                ]
            },
            "openTime": null,
            "closeTime": null,
            "kw": null,
            "meterValueInterval": null,
            "lastHeartbeat": null,
            "lastCsms": null,
            "vendor": null,
            "model": null,
            "enabled": true,
            "pricePerKwh": 20,
            "pricePerMinute": 15,
            "currency": "USD",
            "verified": false,
            "isPremium": false,
            "vehicleType": "2",
            "type": "AC",
            "createdAt": "2022-03-30T07:06:19.945Z",
            "updatedAt": "2022-03-30T07:06:19.945Z",
            "UserId": "user_G2T9AdM",
            "ChargerCommissionGroupId": null,
            "ChargerGroupId": "ChargerGroup_nkSPGA9",
            "Connectors": [
                {
                    "id": "connector_vCSkYsTa2i",
                    "connectorId": 1,
                    "status": "Unavailable",
                    "type": "CCS1",
                    "createdAt": "2022-03-30T07:06:19.971Z",
                    "updatedAt": "2022-03-30T07:06:19.971Z",
                    "ChargerId": "test02"
                }
            ],
            "ChargerGroup": {
                "id": "ChargerGroup_nkSPGA9",
                "name": "Test2",
                "description": "Test station 2",
                "type": "PublicList",
                "createdAt": "2022-03-30T07:04:26.365Z",
                "updatedAt": "2022-03-30T07:04:26.365Z"
            }
        },
        {
            "online": false,
            "id": "PCTY21080002",
            "name": "Chargecore test",
            "description": "vmvh",
            "pincode": "785614",
            "city": "Mandir",
            "address": "China",
            "identification": "Near Namghar",
            "location": {
                "crs": {
                    "type": "name",
                    "properties": {
                        "name": "EPSG:4326"
                    }
                },
                "type": "Point",
                "coordinates": [
                    35.8617,
                    104.1954
                ]
            },
            "openTime": "00:00",
            "closeTime": "24:00",
            "kw": 7.5,
            "meterValueInterval": 12,
            "lastHeartbeat": "2022-02-26T07:40:30.893Z",
            "lastCsms": null,
            "vendor": "",
            "model": "",
            "enabled": true,
            "pricePerKwh": 20,
            "pricePerMinute": 15,
            "currency": "USD",
            "verified": true,
            "isPremium": false,
            "vehicleType": "4",
            "type": "AC",
            "createdAt": "2021-09-27T07:13:38.256Z",
            "updatedAt": "2022-02-26T07:40:30.893Z",
            "UserId": "user_G2T9AdM",
            "ChargerCommissionGroupId": null,
            "ChargerGroupId": "PublicList",
            "Connectors": [
                {
                    "id": "connector_5SAmFbCWhW",
                    "connectorId": 1,
                    "status": "Available",
                    "type": "TYPE2",
                    "createdAt": "2021-09-27T07:13:39.992Z",
                    "updatedAt": "2022-02-26T07:40:25.881Z",
                    "ChargerId": "PCTY21080002"
                },
                {
                    "id": "connector_73ypNDrbO_",
                    "connectorId": 0,
                    "status": "Available",
                    "type": "CCS",
                    "createdAt": "2021-09-28T06:03:53.295Z",
                    "updatedAt": "2021-09-28T12:55:54.389Z",
                    "ChargerId": "PCTY21080002"
                }
            ],
            "ChargerGroup": {
                "id": "PublicList",
                "name": "Test Station",
                "description": "Test",
                "type": "PublicList",
                "createdAt": "2021-10-04T17:48:35.000Z",
                "updatedAt": "2021-10-04T17:48:35.000Z"
            }
        },
        {
            "online": false,
            "id": "charger_GhZX-zDBDn",
            "name": "Priyanku Hazarika Test",
            "description": "Priyanku Hazarika Test",
            "pincode": null,
            "city": null,
            "address": "Kokrajhar",
            "identification": null,
            "location": {
                "crs": {
                    "type": "name",
                    "properties": {
                        "name": "EPSG:4326"
                    }
                },
                "type": "Point",
                "coordinates": [
                    0,
                    0
                ]
            },
            "openTime": null,
            "closeTime": null,
            "kw": null,
            "meterValueInterval": null,
            "lastHeartbeat": null,
            "lastCsms": null,
            "vendor": null,
            "model": null,
            "enabled": true,
            "pricePerKwh": 20,
            "pricePerMinute": 15,
            "currency": "USD",
            "verified": true,
            "isPremium": true,
            "vehicleType": "2",
            "type": "AC",
            "createdAt": "2022-01-28T06:48:49.117Z",
            "updatedAt": "2022-01-28T06:48:49.117Z",
            "UserId": "user_G2T9AdM",
            "ChargerCommissionGroupId": null,
            "ChargerGroupId": "ChargerGroup_w-vqWEY",
            "Connectors": [
                {
                    "id": "connector_J-PS8Mt-A6",
                    "connectorId": 1,
                    "status": "Unavailable",
                    "type": "CCS1",
                    "createdAt": "2022-01-28T06:48:49.216Z",
                    "updatedAt": "2022-01-28T06:48:49.216Z",
                    "ChargerId": "charger_GhZX-zDBDn"
                }
            ],
            "ChargerGroup": {
                "id": "ChargerGroup_w-vqWEY",
                "name": "loist 2",
                "description": "",
                "type": "PublicList",
                "createdAt": "2022-01-26T14:42:08.401Z",
                "updatedAt": "2022-01-26T14:43:18.375Z"
            }
        },
        {
            "online": false,
            "id": "11220795",
            "name": "Test Charger 3 Mar (1)",
            "description": "",
            "pincode": null,
            "city": null,
            "address": "Test",
            "identification": null,
            "location": {
                "crs": {
                    "type": "name",
                    "properties": {
                        "name": "EPSG:4326"
                    }
                },
                "type": "Point",
                "coordinates": [
                    0,
                    0
                ]
            },
            "openTime": null,
            "closeTime": null,
            "kw": null,
            "meterValueInterval": null,
            "lastHeartbeat": "2022-04-07T07:55:02.319Z",
            "lastCsms": null,
            "vendor": "EVRE",
            "model": "10kw",
            "enabled": true,
            "pricePerKwh": 20,
            "pricePerMinute": 15,
            "currency": "USD",
            "verified": false,
            "isPremium": false,
            "vehicleType": "4",
            "type": "AC",
            "createdAt": "2022-03-03T08:35:00.304Z",
            "updatedAt": "2022-04-07T07:55:02.319Z",
            "UserId": "user_G2T9AdM",
            "ChargerCommissionGroupId": null,
            "ChargerGroupId": "PublicList",
            "Connectors": [
                {
                    "id": "connector_DAgwDuW9eQ",
                    "connectorId": 0,
                    "status": "Available",
                    "type": "CCS",
                    "createdAt": "2022-03-16T10:45:39.179Z",
                    "updatedAt": "2022-03-16T11:04:08.065Z",
                    "ChargerId": "11220795"
                },
                {
                    "id": "connector_Z7AtA0sJEk",
                    "connectorId": 1,
                    "status": "Preparing",
                    "type": "CCS1",
                    "createdAt": "2022-03-03T08:35:00.339Z",
                    "updatedAt": "2022-04-07T07:52:15.450Z",
                    "ChargerId": "11220795"
                },
                {
                    "id": "connector_j8u2LTJVhy",
                    "connectorId": 2,
                    "status": "Unavailable",
                    "type": "CCS",
                    "createdAt": "2022-03-16T10:45:43.357Z",
                    "updatedAt": "2022-04-07T07:52:16.897Z",
                    "ChargerId": "11220795"
                }
            ],
            "ChargerGroup": {
                "id": "PublicList",
                "name": "Test Station",
                "description": "Test",
                "type": "PublicList",
                "createdAt": "2021-10-04T17:48:35.000Z",
                "updatedAt": "2021-10-04T17:48:35.000Z"
            }
        },
        {
            "online": false,
            "id": "addefasdfsd",
            "name": "add12",
            "description": "Test desc",
            "pincode": null,
            "city": null,
            "address": "Guwahati",
            "identification": null,
            "location": {
                "type": "Point",
                "coordinates": [
                    11.2521452,
                    91.8576441
                ]
            },
            "openTime": "00:00",
            "closeTime": "24:00",
            "kw": 7.5,
            "meterValueInterval": 12,
            "lastHeartbeat": null,
            "lastCsms": null,
            "vendor": null,
            "model": null,
            "enabled": true,
            "pricePerKwh": 20,
            "pricePerMinute": 15,
            "currency": "USD",
            "verified": true,
            "isPremium": false,
            "vehicleType": "4",
            "type": "DC",
            "createdAt": "2021-10-04T04:48:39.763Z",
            "updatedAt": "2021-10-04T18:07:30.874Z",
            "UserId": "user_G2T9AdM",
            "ChargerCommissionGroupId": null,
            "ChargerGroupId": "PublicList",
            "Connectors": [
                {
                    "id": "connector_jmxs9ZVjgl",
                    "connectorId": 1,
                    "status": "Unavailable",
                    "type": "CCS1",
                    "createdAt": "2021-10-04T04:48:45.187Z",
                    "updatedAt": "2021-10-04T04:48:45.187Z",
                    "ChargerId": "addefasdfsd"
                },
                {
                    "id": "connector_E1RIf7CvQz",
                    "connectorId": 2,
                    "status": "Unavailable",
                    "type": "CCS1",
                    "createdAt": "2021-10-04T04:48:45.187Z",
                    "updatedAt": "2021-10-04T04:48:45.187Z",
                    "ChargerId": "addefasdfsd"
                }
            ],
            "ChargerGroup": {
                "id": "PublicList",
                "name": "Test Station",
                "description": "Test",
                "type": "PublicList",
                "createdAt": "2021-10-04T17:48:35.000Z",
                "updatedAt": "2021-10-04T17:48:35.000Z"
            }
        },
        {
            "online": false,
            "id": "29461782621490",
            "name": "add123",
            "description": "Test ",
            "pincode": "785614",
            "city": "Mandir",
            "address": "Guwahati",
            "identification": "Near Namghar",
            "location": {
                "type": "Point",
                "coordinates": [
                    26.5827672,
                    93.1559421
                ]
            },
            "openTime": "00:00",
            "closeTime": "24:00",
            "kw": 7.5,
            "meterValueInterval": 12,
            "lastHeartbeat": null,
            "lastCsms": null,
            "vendor": null,
            "model": null,
            "enabled": true,
            "pricePerKwh": 20,
            "pricePerMinute": 15,
            "currency": "USD",
            "verified": true,
            "isPremium": false,
            "vehicleType": "4",
            "type": "DC",
            "createdAt": "2021-09-27T09:08:03.910Z",
            "updatedAt": "2021-10-04T17:48:35.000Z",
            "UserId": "user_G2T9AdM",
            "ChargerCommissionGroupId": null,
            "ChargerGroupId": "PublicList",
            "Connectors": [
                {
                    "id": "connector__SkgsV4Y0L",
                    "connectorId": 1,
                    "status": "Unavailable",
                    "type": "CCS2",
                    "createdAt": "2021-09-27T09:08:04.628Z",
                    "updatedAt": "2021-10-04T15:04:09.973Z",
                    "ChargerId": "29461782621490"
                },
                {
                    "id": "connector_imEtZHLtXG",
                    "connectorId": 2,
                    "status": "Unavailable",
                    "type": "CCS2",
                    "createdAt": "2021-10-04T15:05:48.433Z",
                    "updatedAt": "2021-10-04T15:05:48.433Z",
                    "ChargerId": "29461782621490"
                },
                {
                    "id": "connector_ZAp9_eDgc8",
                    "connectorId": 4,
                    "status": "Unavailable",
                    "type": "CCS2",
                    "createdAt": "2021-10-04T15:07:28.384Z",
                    "updatedAt": "2021-10-04T15:07:28.384Z",
                    "ChargerId": "29461782621490"
                },
                {
                    "id": "connector_6si3uCItQK",
                    "connectorId": 3,
                    "status": "Unavailable",
                    "type": "TYPE2",
                    "createdAt": "2021-10-04T15:06:21.852Z",
                    "updatedAt": "2021-10-04T15:07:29.234Z",
                    "ChargerId": "29461782621490"
                }
            ],
            "ChargerGroup": {
                "id": "PublicList",
                "name": "Test Station",
                "description": "Test",
                "type": "PublicList",
                "createdAt": "2021-10-04T17:48:35.000Z",
                "updatedAt": "2021-10-04T17:48:35.000Z"
            }
        },
        {
            "online": false,
            "id": "PCTY2108005505",
            "name": "test charger",
            "description": " you",
            "pincode": "785614",
            "city": "Mandir",
            "address": null,
            "identification": "Near Namghar",
            "location": {
                "crs": {
                    "type": "name",
                    "properties": {
                        "name": "EPSG:4326"
                    }
                },
                "type": "Point",
                "coordinates": [
                    27.1751,
                    78.0421
                ]
            },
            "openTime": "00:00",
            "closeTime": "24:00",
            "kw": 7.5,
            "meterValueInterval": 12,
            "lastHeartbeat": null,
            "lastCsms": null,
            "vendor": null,
            "model": null,
            "enabled": true,
            "pricePerKwh": 20,
            "pricePerMinute": 15,
            "currency": "USD",
            "verified": true,
            "isPremium": false,
            "vehicleType": "4",
            "type": "AC",
            "createdAt": "2022-02-18T18:13:59.439Z",
            "updatedAt": "2022-02-18T18:13:59.439Z",
            "UserId": "user_G2T9AdM",
            "ChargerCommissionGroupId": null,
            "ChargerGroupId": "PublicList",
            "Connectors": [
                {
                    "id": "connector_aLQ8BzDvgP",
                    "connectorId": 1,
                    "status": "Unavailable",
                    "type": "TYPE2",
                    "createdAt": "2022-02-18T18:14:01.284Z",
                    "updatedAt": "2022-02-18T18:14:01.284Z",
                    "ChargerId": "PCTY2108005505"
                }
            ],
            "ChargerGroup": {
                "id": "PublicList",
                "name": "Test Station",
                "description": "Test",
                "type": "PublicList",
                "createdAt": "2021-10-04T17:48:35.000Z",
                "updatedAt": "2021-10-04T17:48:35.000Z"
            }
        },
        {
            "online": false,
            "id": "PCTY21080055dsfgfdsgdu05",
            "name": "test charger",
            "description": " you",
            "pincode": "785614",
            "city": "Mandir",
            "address": null,
            "identification": "Near Namghar",
            "location": {
                "crs": {
                    "type": "name",
                    "properties": {
                        "name": "EPSG:4326"
                    }
                },
                "type": "Point",
                "coordinates": [
                    27.1751,
                    78.0421
                ]
            },
            "openTime": "00:00",
            "closeTime": "24:00",
            "kw": 7.5,
            "meterValueInterval": 12,
            "lastHeartbeat": null,
            "lastCsms": null,
            "vendor": null,
            "model": null,
            "enabled": true,
            "pricePerKwh": 20,
            "pricePerMinute": 15,
            "currency": "USD",
            "verified": true,
            "isPremium": false,
            "vehicleType": "4",
            "type": "AC",
            "createdAt": "2022-02-18T18:18:28.615Z",
            "updatedAt": "2022-02-18T18:18:28.615Z",
            "UserId": "user_G2T9AdM",
            "ChargerCommissionGroupId": null,
            "ChargerGroupId": "PCTY21080055u05",
            "Connectors": [
                {
                    "id": "connector_aPVTjgdspj",
                    "connectorId": 1,
                    "status": "Unavailable",
                    "type": "TYPE2",
                    "createdAt": "2022-02-18T18:18:29.604Z",
                    "updatedAt": "2022-02-18T18:18:29.604Z",
                    "ChargerId": "PCTY21080055dsfgfdsgdu05"
                }
            ],
            "ChargerGroup": {
                "id": "PCTY21080055u05",
                "name": "PCTY21080055u05",
                "description": null,
                "type": "Unlisted",
                "createdAt": "2022-02-18T18:16:44.801Z",
                "updatedAt": "2022-02-18T18:16:44.801Z"
            }
        },
        {
            "online": false,
            "id": "AoNeng0001",
            "name": "Oyika Test Charger",
            "description": "Oyika Test Charger",
            "pincode": null,
            "city": null,
            "address": "testing change",
            "identification": null,
            "location": {
                "crs": {
                    "type": "name",
                    "properties": {
                        "name": "EPSG:4326"
                    }
                },
                "type": "Point",
                "coordinates": [
                    0,
                    0
                ]
            },
            "openTime": null,
            "closeTime": null,
            "kw": null,
            "meterValueInterval": null,
            "lastHeartbeat": "2022-03-09T12:07:29.851Z",
            "lastCsms": null,
            "vendor": "",
            "model": "",
            "enabled": true,
            "pricePerKwh": 20,
            "pricePerMinute": 15,
            "currency": "USD",
            "verified": true,
            "isPremium": false,
            "vehicleType": "4",
            "type": "DC",
            "createdAt": "2021-11-22T06:53:27.249Z",
            "updatedAt": "2022-03-09T12:07:29.852Z",
            "UserId": "user_G2T9AdM",
            "ChargerCommissionGroupId": null,
            "ChargerGroupId": "PublicList",
            "Connectors": [
                {
                    "id": "connector_1F-Y6PxGfu",
                    "connectorId": 1,
                    "status": "Available",
                    "type": "CCS1",
                    "createdAt": "2021-11-22T06:53:27.311Z",
                    "updatedAt": "2022-03-09T12:03:45.689Z",
                    "ChargerId": "AoNeng0001"
                }
            ],
            "ChargerGroup": {
                "id": "PublicList",
                "name": "Test Station",
                "description": "Test",
                "type": "PublicList",
                "createdAt": "2021-10-04T17:48:35.000Z",
                "updatedAt": "2021-10-04T17:48:35.000Z"
            }
        },
        {
            "online": false,
            "id": "customerchargerid1",
            "name": "test charger",
            "description": " you",
            "pincode": "785614",
            "city": "Mandir",
            "address": null,
            "identification": "Near Namghar",
            "location": {
                "crs": {
                    "type": "name",
                    "properties": {
                        "name": "EPSG:4326"
                    }
                },
                "type": "Point",
                "coordinates": [
                    27.1751,
                    78.0421
                ]
            },
            "openTime": "00:00",
            "closeTime": "24:00",
            "kw": 7.5,
            "meterValueInterval": 12,
            "lastHeartbeat": null,
            "lastCsms": null,
            "vendor": null,
            "model": null,
            "enabled": true,
            "pricePerKwh": 20,
            "pricePerMinute": 15,
            "currency": "USD",
            "verified": false,
            "isPremium": false,
            "vehicleType": "4",
            "type": "AC",
            "createdAt": "2022-04-09T06:56:50.385Z",
            "updatedAt": "2022-04-09T06:56:50.385Z",
            "UserId": null,
            "ChargerCommissionGroupId": null,
            "ChargerGroupId": "PCTY21080055u05",
            "Connectors": [
                {
                    "id": "connector_1-dQ2IMI1K",
                    "connectorId": 1,
                    "status": "Unavailable",
                    "type": "TYPE2",
                    "createdAt": "2022-04-09T06:56:50.984Z",
                    "updatedAt": "2022-04-09T06:56:50.984Z",
                    "ChargerId": "customerchargerid1"
                }
            ],
            "ChargerGroup": {
                "id": "PCTY21080055u05",
                "name": "PCTY21080055u05",
                "description": null,
                "type": "Unlisted",
                "createdAt": "2022-02-18T18:16:44.801Z",
                "updatedAt": "2022-02-18T18:16:44.801Z"
            }
        },
        {
            "online": false,
            "id": "hhhvh",
            "name": "klkj",
            "description": "vmvh",
            "pincode": null,
            "city": null,
            "address": "gyujgulh",
            "identification": null,
            "location": {
                "type": "Point",
                "coordinates": [
                    1.366596,
                    103.8246711
                ]
            },
            "openTime": "00:00",
            "closeTime": "24:00",
            "kw": 7.5,
            "meterValueInterval": null,
            "lastHeartbeat": null,
            "lastCsms": null,
            "vendor": null,
            "model": null,
            "enabled": true,
            "pricePerKwh": 20,
            "pricePerMinute": 15,
            "currency": "USD",
            "verified": true,
            "isPremium": false,
            "vehicleType": "4",
            "type": "DC",
            "createdAt": "2021-09-27T07:01:55.337Z",
            "updatedAt": "2021-09-27T07:01:55.337Z",
            "UserId": "user_DX3W3SU",
            "ChargerCommissionGroupId": null,
            "ChargerGroupId": "PublicList",
            "Connectors": [],
            "ChargerGroup": {
                "id": "PublicList",
                "name": "Test Station",
                "description": "Test",
                "type": "PublicList",
                "createdAt": "2021-10-04T17:48:35.000Z",
                "updatedAt": "2021-10-04T17:48:35.000Z"
            }
        }
    ],
    "status": 200
}
```
{% endswagger-response %}

{% swagger-response status="401" description="Permission denied" %}

{% endswagger-response %}
{% endswagger %}

{% tabs %}
{% tab title="HTTP" %}
```
GET /users/charger HTTP/1.1
Host: https://csms-api.tomsawyer-energy.com
Authorization: Bearer x.x.x
Content-Type: application/json
Content-Length: 67

```
{% endtab %}

{% tab title="Python" %}
```
import requests

url = "https://csms-api.tomsawyer-energy.com/users/charger/"

payload = ""
headers = {
  'Authorization': 'Bearer x.x.x'
}

response = requests.request("GET", url, headers=headers, data=payload)

print(response.text)
```
{% endtab %}
{% endtabs %}

## Fetch single charger

{% swagger baseUrl="/users/charger" method="get" path="" summary="Fetch charger." %}
{% swagger-description %}





{% endswagger-description %}

{% swagger-parameter in="path" required="true" %}
Charger id
{% endswagger-parameter %}

{% swagger-response status="200" description="successfully fetch" %}
```javascript
{
    "error": null,
    "count": 1,
    "data": {
        "online": false,
        "id": "customerchargerid1",
        "name": "test charger",
        "description": " you",
        "pincode": "785614",
        "city": "ccc",
        "address": null,
        "identification": "Near Mandir",
        "location": {
            "crs": {
                "type": "name",
                "properties": {
                    "name": "EPSG:4326"
                }
            },
            "type": "Point",
            "coordinates": [
                27.1751,
                78.0421
            ]
        },
        "openTime": "00:00",
        "closeTime": "24:00",
        "kw": 7.5,
        "meterValueInterval": 12,
        "lastHeartbeat": null,
        "lastCsms": null,
        "vendor": null,
        "model": null,
        "enabled": true,
        "pricePerKwh": 20,
        "pricePerMinute": 15,
        "currency": "USD",
        "verified": false,
        "isPremium": false,
        "vehicleType": "4",
        "type": "AC",
        "createdAt": "2022-04-09T06:56:50.385Z",
        "updatedAt": "2022-04-09T06:56:50.385Z",
        "UserId": null,
        "ChargerCommissionGroupId": null,
        "ChargerGroupId": "PCTY21080055u05",
        "Connectors": [
            {
                "id": "connector_1-dQ2IMI1K",
                "connectorId": 1,
                "status": "Unavailable",
                "type": "TYPE2",
                "createdAt": "2022-04-09T06:56:50.984Z",
                "updatedAt": "2022-04-09T06:56:50.984Z",
                "ChargerId": "customerchargerid1"
            }
        ]
    },
    "status": 200
}
```
{% endswagger-response %}

{% swagger-response status="401" description="Permission denied" %}

{% endswagger-response %}
{% endswagger %}



{% tabs %}
{% tab title="HTTP" %}
```
GET /users/charger/chargerid HTTP/1.1
Host: https://csms-api.tomsawyer-energy.com
Authorization: Bearer x.x.x
Content-Type: application/json
Content-Length: 67

```
{% endtab %}

{% tab title="Python" %}
```
import requests

url = "https://csms-api.tomsawyer-energy.com/users/charger/<chargerid>"

payload = ""
headers = {
  'Authorization': 'Bearer x.x.x'
}

response = requests.request("GET", url, headers=headers, data=payload)

print(response.text)
```
{% endtab %}
{% endtabs %}

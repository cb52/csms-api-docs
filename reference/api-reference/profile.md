# 🧑 Profile

### GET

{% swagger method="get" path="" baseUrl="/users/profile" summary="" %}
{% swagger-description %}

{% endswagger-description %}

{% swagger-response status="200: OK" description="successfully done" %}
```javascript

{
    "error": null,
    "count": 1,
    "data": {
        "id": "user_G2T9AdM",
        "name": "Name ",
        "needToChangePassword": false,
        "phone": "+34567890",
        "phoneVerified": false,
        "email": "qwertyui@gmail.com",
        "emailVerified": true,
        "googleUserId": null,
        "facebookUserId": null,
        "paymentGatewayCustomerId": "cus_Ky4HGv9RhN9TbS",
        "address": "indidad",
        "city": "Chennai",
        "pincode": "123454",
        "createdAt": "2021-09-13T17:36:25.095Z",
        "updatedAt": "2022-05-18T09:35:12.546Z",
        "MediumId": "media_8qseELB",
        "HostApprovalId": null,
        "Tester": null,
        "Assignee": null,
        "Medium": {
            "domain": "https://tomsawyer-staging.s3.ap-south-1.amazonaws.com",
            "id": "media_8qseELB",
            "key": "4TdMYJLa8GvgxFPpnHjP.jpg",
            "s3Link": "4TdMYJLa8GvgxFPpnHjP",
            "extension": "jpg",
            "createdAt": "2022-05-17T09:29:09.816Z",
            "updatedAt": "2022-05-17T09:29:09.816Z"
        },
        "role": []
    },
    "status": 200
}
```
{% endswagger-response %}

{% swagger-response status="400: Bad Request" description="" %}
```javascript
{
    // Response
}
```
{% endswagger-response %}

{% swagger-response status="401: Unauthorized" description="" %}
```javascript
{
    // Response
}
```
{% endswagger-response %}

{% swagger-response status="500: Internal Server Error" description="" %}
```javascript
{
    // Response
}
```
{% endswagger-response %}
{% endswagger %}



{% tabs %}
{% tab title="HTTP" %}
```
GET /users/profile HTTP/1.1
Host: csms-api.tomsawyer-energy.com
Authorization: Bearer x.x.x

```
{% endtab %}

{% tab title="Python" %}
```
import http.client

conn = http.client.HTTPSConnection("csms-api.tomsawyer-energy.com")
payload = ''
headers = {
  'Authorization': 'Bearer x.x.x'
}
conn.request("GET", "/users/profile", payload, headers)
res = conn.getresponse()
data = res.read()
print(data.decode("utf-8"))
```
{% endtab %}
{% endtabs %}

### Update Phone No.

{% swagger method="post" path="" baseUrl="/common/auth/sendOtp" summary="" %}
{% swagger-description %}

{% endswagger-description %}

{% swagger-parameter in="body" name="otpType" %}
CHANGE_PHONE_VERIFICATION
{% endswagger-parameter %}

{% swagger-parameter in="body" name="phone" %}
\+91 9876543210
{% endswagger-parameter %}

{% swagger-response status="200: OK" description="" %}
```javascript
{
    "error": null,
    "count": 1,
    "data": {
        "message": "success"
    },
    "status": 200
}
```
{% endswagger-response %}

{% swagger-response status="400: Bad Request" description="" %}
```javascript
{
    // Response
}
```
{% endswagger-response %}

{% swagger-response status="401: Unauthorized" description="" %}
```javascript
{
    "error": "Invalid AccessToken",
    "data": null,
    "status": 401
}
```
{% endswagger-response %}
{% endswagger %}

{% tabs %}
{% tab title="HTTP" %}
```
POST /common/auth/sendOtp HTTP/1.1
Host: csms-api.tomsawyer-energy.com
Authorization: Bearer Bearer x.x.x
Content-Type: application/json
Content-Length: 80

{
    "otpType":"CHANGE_PHONE_VERIFICATION",
    "phone": "+918931806160"
}
```
{% endtab %}

{% tab title="Python" %}
```
import requests
import json

url = "csms-api.tomsawyer-energy.com/common/auth/sendOtp"

payload = json.dumps({
  "otpType": "CHANGE_PHONE_VERIFICATION",
  "phone": "+916900928174"
})
headers = {
  'Content-Type': 'application/json'
}

response = requests.request("POST", url, headers=headers, data=payload)

print(response.text)

```
{% endtab %}
{% endtabs %}

### Verify Updated Phone No. with OTP

{% swagger method="get" path="" baseUrl="/common/auth/verifyChangePhone?code=1234" summary="" %}
{% swagger-description %}

{% endswagger-description %}

{% swagger-response status="200: OK" description="" %}
```javascript
{
    "error": null,
    "count": 1,
    "data": {
        "message": "success"
    },
    "status": 200
}
```
{% endswagger-response %}

{% swagger-response status="401: Unauthorized" description="" %}
```javascript
{
    "error": {
        "name": "JsonWebTokenError",
        "message": "invalid algorithm"
    },
    "data": null,
    "status": 401
}
```
{% endswagger-response %}
{% endswagger %}

{% tabs %}
{% tab title="HTTP" %}
```
GET /common/auth/verifyChangePhone?code=1758 HTTP/1.1
Host: csms-api.tomsawyer-energy.com
Authorization: Bearer x.x.x
```
{% endtab %}

{% tab title="Python" %}
```
import requests

url = "csms-api.tomsawyer-energy.com/common/auth/verifyChangePhone?code=9847"

payload = ""
headers = {
  'Authorization': 'Bearer x.x.x'
}

response = requests.request("GET", url, headers=headers, data=payload)

print(response.text)

```
{% endtab %}
{% endtabs %}

### Update Profile

{% swagger method="put" path="" baseUrl="/users/profile" summary="" %}
{% swagger-description %}

{% endswagger-description %}

{% swagger-parameter in="body" name="name" type="String" %}

{% endswagger-parameter %}

{% swagger-parameter in="body" name="email" type="String" %}

{% endswagger-parameter %}

{% swagger-parameter in="body" name="phone" type="String" %}

{% endswagger-parameter %}

{% swagger-parameter in="body" name="address" type="String" %}

{% endswagger-parameter %}

{% swagger-parameter in="body" name="city" type="String" %}

{% endswagger-parameter %}

{% swagger-parameter in="body" name="pincode" type="String" %}

{% endswagger-parameter %}

{% swagger-response status="200: OK" description="successfully done" %}
```javascript

{
    "error": null,
    "count": 1,
    "data": {
        "id": "user_G2T9AdM",
        "name": "Name ",
        "needToChangePassword": false,
        "phone": "+34567890",
        "phoneVerified": false,
        "email": "qwertyui@gmail.com",
        "emailVerified": true,
        "googleUserId": null,
        "facebookUserId": null,
        "paymentGatewayCustomerId": "cus_Ky4HGv9RhN9TbS",
        "address": "indidad",
        "city": "Chennai",
        "pincode": "123454",
        "createdAt": "2021-09-13T17:36:25.095Z",
        "updatedAt": "2022-05-18T09:35:12.546Z",
        "MediumId": "media_8qseELB",
        "HostApprovalId": null,
        "Tester": null,
        "Assignee": null,
        "Medium": {
            "domain": "https://tomsawyer-staging.s3.ap-south-1.amazonaws.com",
            "id": "media_8qseELB",
            "key": "4TdMYJLa8GvgxFPpnHjP.jpg",
            "s3Link": "4TdMYJLa8GvgxFPpnHjP",
            "extension": "jpg",
            "createdAt": "2022-05-17T09:29:09.816Z",
            "updatedAt": "2022-05-17T09:29:09.816Z"
        },
        "role": []
    },
    "status": 200
}
```
{% endswagger-response %}

{% swagger-response status="400: Bad Request" description="" %}
```javascript
{
    // Response
}
```
{% endswagger-response %}

{% swagger-response status="401: Unauthorized" description="" %}
```javascript
{
    // Response
}
```
{% endswagger-response %}

{% swagger-response status="500: Internal Server Error" description="" %}
```javascript
{
    // Response
}
```
{% endswagger-response %}
{% endswagger %}

{% tabs %}
{% tab title="HTTP" %}
```
PUT //users/profile HTTP/1.1
Host: csms-api.tomsawyer-energy.com
Authorization: Bearer Bearer x.x.x
Content-Type: application/json
Content-Length: 108

{
    "name": "x x New Name",
    "email": "x@gmail.com",
    "phone": "+911234567890"
}
```
{% endtab %}

{% tab title="Python" %}
```
import http.client
import json

conn = http.client.HTTPSConnection("csms-api.tomsawyer-energy.com")
payload = json.dumps({
  "name": "x x New Name",
  "email": "x@gmail.com",
  "phone": "+919876543210"
})
headers = {
  'Authorization': 'Bearer Bearer x.x.x',
  'Content-Type': 'application/json'
}
conn.request("PUT", "/users/profile", payload, headers)
res = conn.getresponse()
data = res.read()
print(data.decode("utf-8"))
```
{% endtab %}
{% endtabs %}

### Update Profile Pic

{% swagger method="put" path="" baseUrl="/users/profile/image" summary="" %}
{% swagger-description %}

{% endswagger-description %}

{% swagger-parameter in="body" name="data" required="true" %}
base64 image data
{% endswagger-parameter %}

{% swagger-response status="200: OK" description="successfully done" %}
```javascript

{
    "error": null,
    "count": 1,
    "data": {
        "id": "user_G2T9AdM",
        "name": "Name ",
        "needToChangePassword": false,
        "phone": "+34567890",
        "phoneVerified": false,
        "email": "qwertyui@gmail.com",
        "emailVerified": true,
        "googleUserId": null,
        "facebookUserId": null,
        "paymentGatewayCustomerId": "cus_Ky4HGv9RhN9TbS",
        "address": "indidad",
        "city": "Chennai",
        "pincode": "123454",
        "createdAt": "2021-09-13T17:36:25.095Z",
        "updatedAt": "2022-05-18T09:35:12.546Z",
        "MediumId": "media_8qseELB",
        "HostApprovalId": null,
        "Tester": null,
        "Assignee": null,
        "Medium": {
            "domain": "https://tomsawyer-staging.s3.ap-south-1.amazonaws.com",
            "id": "media_8qseELB",
            "key": "4TdMYJLa8GvgxFPpnHjP.jpg",
            "s3Link": "4TdMYJLa8GvgxFPpnHjP",
            "extension": "jpg",
            "createdAt": "2022-05-17T09:29:09.816Z",
            "updatedAt": "2022-05-17T09:29:09.816Z"
        },
        "role": []
    },
    "status": 200
}
```
{% endswagger-response %}

{% swagger-response status="400: Bad Request" description="" %}
```javascript
{
    // Response
}
```
{% endswagger-response %}

{% swagger-response status="401: Unauthorized" description="" %}
```javascript
{
    // Response
}
```
{% endswagger-response %}

{% swagger-response status="500: Internal Server Error" description="" %}
```javascript
{
    // Response
}
```
{% endswagger-response %}
{% endswagger %}

{% tabs %}
{% tab title="HTTP" %}
```
PUT //users/profile/image HTTP/1.1
Host: csms-api.tomsawyer-energy.com
Authorization: Bearer Bearer x.x.x
Content-Type: application/json
Content-Length: 17

{
    "data":""
}

```
{% endtab %}

{% tab title="Python" %}
```
import requests
import json

url = "csms-api.tomsawyer-energy.com/users/profile/image"

payload = json.dumps({
  "data": "data:image/jpeg;base64,xxx"
})
headers = {
  'Authorization': 'Bearer Bearer x.x.x',
  'Content-Type': 'application/json'
}

response = requests.request("PUT", url, headers=headers, data=payload)

print(response.text)

```
{% endtab %}
{% endtabs %}

{% swagger method="delete" path="" baseUrl="/users/profile/image" summary="" %}
{% swagger-description %}

{% endswagger-description %}

{% swagger-response status="204: No Content" description="successfully done" %}
```javascript
{
    // Response
}
```
{% endswagger-response %}

{% swagger-response status="400: Bad Request" description="" %}
```javascript
{
    // Response
}
```
{% endswagger-response %}

{% swagger-response status="401: Unauthorized" description="" %}
```javascript
{
    // Response
}
```
{% endswagger-response %}

{% swagger-response status="500: Internal Server Error" description="" %}
```javascript
{
    // Response
}
```
{% endswagger-response %}
{% endswagger %}

{% tabs %}
{% tab title="HTTP" %}
```
DELETE //users/profile/image HTTP/1.1
Host: csms-api.tomsawyer-energy.com
Authorization: Bearer x.x.x

```
{% endtab %}

{% tab title="Python" %}
```
import requests

url = "http://csms-api.tomsawyer-energy.com/users/profile/image"

payload = ""
headers = {
  'Authorization': 'Bearer x.x.x'
}

response = requests.request("DELETE", url, headers=headers, data=payload)

print(response.text)

```
{% endtab %}
{% endtabs %}

